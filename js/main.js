$(document).ready(function(){

	/*==========================
	FitVids Initialization
	For repsonsive video embeds
	==========================*/

 	$('.homepage .video_container').fitVids();

 	/*==========================
	CELEBRITY IMAGE SWAP
	==========================*/

	$('.celebrity_names ul li').click(function(){

		// Run this function only if the link isn't already active
		
		if(!$(this).hasClass('active')) {
			// Save the name of the celebrity that was clicked
			var name = $(this).attr('data-name');

			// FadeIn the clicked celebrity image
			$('.homepage_celebrities img#' + name).fadeTo('slow', 1);

			// FadeOut the active celebrity image. Remove the visible status and add it to the new clicked image
			$('.homepage_celebrities img.visible').fadeTo('slow', 0, function(){
				$(this).removeClass('visible');
				$('.homepage_celebrities img#' + name).addClass('visible');
			});
		}

		// Remove the active status from all links
		$('.celebrity_names ul li').removeClass('active');

		// Add the active status to the name that was clicked
		$(this).addClass('active');

	});

	/*==========================
	QUOTE SLIDER
	Fading quotes functions using flexslider (https://www.woothemes.com/flexslider/)
	==========================*/

	$('.quote_slider').flexslider({
		directionNav: false
	});

	/*===================== 
    Browser Detection // For now hide the HD thumbnail for Safari 
    Only uncomment this if playbutton stops working in Safari. Funky interaction in safari with youtube API

    if (bowser.safari) {
      	$('button.play_button').click(function(){
			$(this).closest('.video_container').find('.module_content').fadeOut('slow', function(){
				$(this).closest('.video_cover').delay(200).fadeOut('slow');
			});
		});
    }

    ===================== */

	/*==========================
	QUOTE TWEET
	==========================*/

	$('a.twitter_share').click(function(e){

	    e.preventDefault();

	    var loc = $(this).attr('href');

	    var via = 'tribeca';

	    // Get the quote and author text for a specific quote

	    var quote = $(this).closest('li').find('.quote_text').text();

	    var author = $(this).closest('li').find('.quote_footer .quote_author').text();

	    // Join the quote and author. Encode the text so it can be used in the twitter share url.

	    var encodedTitle = encodeURIComponent(quote + ' - ' + author);

	    window.open('http://twitter.com/share?url=' + loc + '&via=' + via + '&text=' + encodedTitle, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

	});

});

/*==========================
Youtube Video Controls
==========================*/

var homepageVideo;
var recapVideo;

// Function once Youtube API is ready

function onYouTubePlayerAPIReady() {
	homepageVideo = new YT.Player('homepageVideo', {
		events: {
		  'onReady': onPlayerReady
		}
	});
	recapVideo = new YT.Player('recapVideo', {
		events: {
		  'onReady': onPlayerReady
		}
	});
}

// Function once specific homepage video is ready

function onPlayerReady(event) {
	$('button.play_button').click(function(){
		// Save the ID of the video that was clicked
		var youtubeVideoID = $(this).closest('.video_container').find('iframe').attr('ID');
		// Hide the video cover and play the video
		$(this).closest('.video_cover').delay(800).fadeOut('slow', function(){
			if(youtubeVideoID === 'homepageVideo') {
				homepageVideo.playVideo();
			}
			else if (youtubeVideoID === 'recapVideo') {
				recapVideo.playVideo();
			}
		});
	});
}

// Inject YouTube API script

var tag = document.createElement('script');
tag.src = "//www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);